import React from 'react';
import styled from 'styled-components';
import Pic from '../../../asset/explore/munnar.jpg';
import { Link } from 'react-router-dom';

const TopSpots = ({ data }) => {
	return (
		<TopspotContainer to={data.link}>
			<ImageConatiner>
				<Picture src={data.pic} />
			</ImageConatiner>

			<ContentArea>
				<Title>{data.place}</Title>
			</ContentArea>
		</TopspotContainer>
	);
};

export default TopSpots;

const TopspotContainer = styled(Link)`
	display: flex;
	justify-content: flex-start;
	align-items: center;
`;
const ImageConatiner = styled.div`
	width: 60px;
	min-width: 60px;
	margin-right: 15px;
	overflow: hidden;
`;
const Picture = styled.img`
	width: 100%;
	border-radius: 7px;
`;
const ContentArea = styled.div``;
const Title = styled.h3`
	font-weight: 400;
	font-size: 16px;
`;
const Price = styled.p``;
const Time = styled.span``;
