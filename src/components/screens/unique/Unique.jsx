import React from "react";
import ImaginativeStay from "./ImaginativeStay";
import NearbyStay from "./NearbyStay";
import Spotlight from "./Spotlight";
import UniqueStaysThumnail from "./UniqueStaysThumnail";

const Unique = () => {
    return (
        <>
            <Spotlight />
            <UniqueStaysThumnail />
            <NearbyStay />
            <ImaginativeStay />
        </>
    );
};

export default Unique;
