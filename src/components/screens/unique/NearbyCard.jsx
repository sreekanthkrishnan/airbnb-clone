import React from "react";
import styled from "styled-components";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import GradeIcon from "@material-ui/icons/Grade";

const NearbyCard = (props) => {
    // const data = {
    //     description: "The Mudhouse Marayoor - Cob (Nature Living)",
    // };
    function truncate(str, n) {
        return str.length > n ? str.substring(0, n - 1) + "..." : str;
    }
    return (
        <Card>
            <Label>
                <Button>Superhost</Button>
                <Heart></Heart>
            </Label>
            <CardLink href="#">
                <ImageCover>
                    <Image src={props.src}></Image>
                </ImageCover>
                <ContentArea>
                    <ContentLabel>
                        <Star></Star>
                        <Rating>
                            {props.rating}
                            <ReviewCount>({props.reviews})</ReviewCount>
                        </Rating>
                    </ContentLabel>
                    <Title>{props.title}</Title>
                    <Description>{truncate(props.description, 33)}</Description>
                    <PriceContainer>
                        <Price>&#8377; {props.price}</Price>/night
                    </PriceContainer>
                </ContentArea>
            </CardLink>
        </Card>
    );
};

export default NearbyCard;
const Card = styled.div`
    width: 20%;
    position: relative;
    margin-right: 15px;
    min-width: 315px;
    display: block;
    cursor: pointer;
`;
const CardLink = styled.a`
    display: block;
`;
const ImageCover = styled.div`
    width: 100%;
    height: 200px;
    overflow: hidden;
    border-radius: 10px;
`;
const Image = styled.img`
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
`;
const Label = styled.div`
    position: absolute;
    top: 10px;
    left: 0;
    padding: 0 10px;
    display: flex;
    justify-content: space-between;
    width: 100%;
`;

const Button = styled.button`
    text-transform: uppercase;
    font-weight: 600;
    outline: none;
    border: none;
    padding: 3px 5px;
    background-color: #fff;
    border-radius: 2px;
    cursor: pointer !important;
`;
const Heart = styled(FavoriteBorderIcon)`
    color: #fff !important;
    font-size: 20px;
    cursor: pointer;
`;
const ContentArea = styled.div`
    padding: 10px 5px;
`;

const ContentLabel = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 5px;
`;
const Star = styled(GradeIcon)`
    font-size: 17px !important;
    margin-right: 3px;
    color: #f4385c !important;
`;
const Rating = styled.span`
    display: block;
`;
const ReviewCount = styled.small`
    margin-left: 3px;
    font-size: 15px;
    color: #8c8c8b;
`;
const Title = styled.h3`
    font-weight: 400;
    font-size: 16px;
    color: #484848;
`;
const Description = styled.p`
    font-weight: 400;
    font-size: 16px;
    color: #484848;
`;
const PriceContainer = styled.span`
    display: block;
    width: 100%;
    font-weight: 400;
    font-size: 15px;
    color: #484848;
    margin-top: 10px;
`;
const Price = styled.small`
    font-size: 18px;
    font-weight: 600;
    color: #111;
`;
