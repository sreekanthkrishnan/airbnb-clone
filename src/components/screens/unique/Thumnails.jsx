import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

// const cardDetails

const Thumnails = ({ src, title }) => {
    return (
        <ThumnailCard to="#">
            <Image src={src} alt="Thumnail image"></Image>
            <Title>{title}</Title>
        </ThumnailCard>
    );
};

export default Thumnails;
const ThumnailCard = styled(Link)`
    width: 13%;
    background-color: #fff;
    display: block;
    box-shadow: -1px -1px 18px -7px rgba(0, 0, 0, 0.48);
    border-radius: 15px;
    overflow: hidden;
    margin-top: 20px;
`;
const Image = styled.img`
    display: block;
    width: 100%;
`;
const Title = styled.h4`
    font-size: 15px;
    font-weight: 600;
    padding: 16px;
`;
