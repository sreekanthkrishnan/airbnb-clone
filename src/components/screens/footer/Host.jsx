import React from "react";
import styled from "styled-components";

const Host = (props) => {
    return <Link href="#">{props.links}</Link>;
};

export default Host;
const Link = styled.a`
    display: block;
    width: 100%;
    margin-bottom: 20px;
    font-size: 15px;
    cursor: pointer;
    &:hover {
        color: #73726f;
    }
`;
