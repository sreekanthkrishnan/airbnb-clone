import React from "react";
import styled from "styled-components";

const About = (props) => {
    return <Link href="#">{props.links}</Link>;
};

export default About;
const Link = styled.a`
    display: block;
    width: 100%;
    margin-bottom: 20px;
    font-size: 15px;
    cursor: pointer;
    &:hover {
        color: #73726f;
    }
`;
