import React from "react";
import styled from "styled-components";

const Gallery = () => {
    return (
        <GalleryContainer>
            <LeftSection>
                <Item>
                    <Image src="https://a0.muscache.com/im/pictures/78010337-07f0-4154-9528-363b97b54699.jpg?im_w=720"></Image>
                    <Description>
                        Learn to make soup dumplings in Shanghai
                    </Description>
                </Item>
            </LeftSection>
            <RightSection>
                <TopSection>
                    <TopItem>
                        <Image src="https://a0.muscache.com/im/pictures/0735e435-3d1d-4aec-b536-9ee54f299ce6.jpg?im_w=720"></Image>
                        <Description>
                            Explore feminism with street art and graffiti
                        </Description>
                    </TopItem>
                    <TopItem>
                        <Image src="https://a0.muscache.com/im/pictures/1793b6aa-4c3c-4193-a65a-09b440b2ca2c.jpg?im_w=720"></Image>
                        <Description>
                            Go backstage with a New York magician
                        </Description>
                    </TopItem>
                </TopSection>
                <BottomSection>
                    <Item>
                        <Image src="https://a0.muscache.com/im/pictures/925f99bb-c5bc-4d82-9803-518abeef7e2e.jpg?im_w=720"></Image>
                        <Description>
                            Craft cocktails and talk gender with a bartender
                        </Description>
                    </Item>
                </BottomSection>
            </RightSection>
        </GalleryContainer>
    );
};

export default Gallery;
const GalleryContainer = styled.div`
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
`;
const Item = styled.div`
    position: relative;
    border-radius: 20px;
    overflow: hidden;
`;
const Image = styled.img`
    display: block;
    width: 100%;
`;
const Description = styled.p`
    color: #fff;
    background-color: #212121;
    position: absolute;
    left: 0;
    bottom: -5px;
    width: 100%;
    padding: 10px 20px 30px;
`;
const LeftSection = styled.div`
    width: 49%;
`;
const RightSection = styled.div`
    width: 49%;
`;
const TopSection = styled.div`
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    margin-bottom: 30px;
`;
const TopItem = styled.div`
    position: relative;
    width: 49%;
    border-radius: 20px;
    overflow: hidden;
`;
const BottomSection = styled.div``;
