import { Button } from "@material-ui/core";
import React, { useState } from "react";
import styled from "styled-components";
import DestinationList from "./DestinationList";

const Destination = () => {
    const [destinations, setDestinations] = useState([
        {
            id: 1,
            city: "Thrivanthapuram",
            state: "Kerala",
            type: "art_culture",
        },
        {
            id: 2,
            city: "Kottayam",
            state: "Kerala",
            type: "outdoor",
        },
        { id: 3, city: "kochi", state: "Kerala", type: "art_culture" },
        { id: 4, city: "Kozhicode", state: "Kerala", type: "outdoor" },
        { id: 5, city: "Malapuram", state: "Kerala", type: "art_culture" },
        { id: 6, city: "Idukki", state: "Kerala", type: "art_culture" },
        { id: 7, city: "Wayanad", state: "Kerala", type: "art_culture" },
        { id: 8, city: "Thrisur", state: "Kerala", type: "art_culture" },
        { id: 9, city: "Palakkad", state: "Kerala", type: "art_culture" },
        { id: 10, city: "Kollam", state: "Kerala", type: "mountain" },
        {
            id: 11,
            city: "Pathanamthitta",
            state: "Kerala",
            type: "art_culture",
        },
        { id: 12, city: "Kasorcod", state: "Kerala", type: "art_culture" },
        { id: 13, city: "Kannur", state: "Kerala", type: "art_culture" },
        { id: 14, city: "Vagattor", state: "Goa", type: "Beach" },
        { id: 14, city: "Vagattor", state: "Goa", type: "Beach" },
        { id: 14, city: "Baga", state: "Goa", type: "Beach" },
        { id: 14, city: "Kovalam", state: "Kerala", type: "Beach" },
        { id: 14, city: "Kottakunne", state: "Kerala", type: "mountain" },
        { id: 14, city: "Ponmudi", state: "Kerala", type: "mountain" },
        { id: 14, city: "Munnar", state: "Kerala", type: "popular" },
        { id: 14, city: "Wagamon", state: "Kerala", type: "mountain" },
        { id: 14, city: "Varkala", state: "Kerala", type: "popular" },
        { id: 12, city: "Kasorcod", state: "Kerala", type: "art_culture" },
        { id: 13, city: "Kannur", state: "Kerala", type: "popular" },
        { id: 14, city: "Vagattor", state: "Goa", type: "Beach" },
        { id: 14, city: "Vagattor", state: "Goa", type: "Beach" },
        { id: 14, city: "Baga", state: "Goa", type: "Beach" },
        { id: 14, city: "Kovalam", state: "Kerala", type: "Beach" },
        { id: 14, city: "Kottakunne", state: "Kerala", type: "mountain" },
        { id: 14, city: "Ponmudi", state: "Kerala", type: "popular" },
        { id: 14, city: "Munnar", state: "Kerala", type: "mountain" },
        { id: 14, city: "Wagamon", state: "Kerala", type: "popular" },
        { id: 14, city: "Varkala", state: "Kerala", type: "Beach" },
    ]);

    // const renderDestinationsList = destinations.map((destination) => (
    //     <DestinationList destination={destination} />
    // ));

    const [destinationFilterType, setDestinationFilterType] = useState("art_culture");

    return (
        <DestinationContainer>
            <DestinationTop>
                <Title>Destinations for future trips</Title>
                <ButtonContainer>
                    <DestinationButton
                        className={destinationFilterType === "art_culture" && "active"}
                        onClick={() => setDestinationFilterType("art_culture")}
                    >
                        Destination for arts & culture
                    </DestinationButton>
                    <DestinationButton
                        className={destinationFilterType === "outdoor" && "active"}
                        onClick={() => setDestinationFilterType("outdoor")}
                    >
                        Destination for outdoor adventures
                    </DestinationButton>
                    <DestinationButton
                        className={destinationFilterType === "mountain" && "active"}
                        onClick={() => setDestinationFilterType("mountain")}
                    >
                        Destination for Moutains
                    </DestinationButton>
                    <DestinationButton
                        className={destinationFilterType === "Beach" && "active"}
                        onClick={() => setDestinationFilterType("Beach")}
                    >
                        Beach Destination
                    </DestinationButton>
                    <DestinationButton
                        className={destinationFilterType === "popular" && "active"}
                        onClick={() => setDestinationFilterType("popular")}
                    >
                        Popular Destination
                    </DestinationButton>
                </ButtonContainer>
            </DestinationTop>
            <DestinationBottomContainer>
                <DestinationBottom>
                    {destinationFilterType === "art_culture"
                        ? destinations
                              .filter((destination) => destination.type === "art_culture")
                              .map((filteredDestination) => (
                                  <DestinationList destination={filteredDestination} />
                              ))
                        : destinationFilterType === "mountain"
                        ? destinations
                              .filter((destination) => destination.type === "mountain")
                              .map((filteredDestination) => (
                                  <DestinationList destination={filteredDestination} />
                              ))
                        : destinationFilterType === "outdoor"
                        ? destinations
                              .filter((destination) => destination.type === "outdoor")
                              .map((filteredDestination) => (
                                  <DestinationList destination={filteredDestination} />
                              ))
                        : destinationFilterType === "Beach"
                        ? destinations
                              .filter((destination) => destination.type === "Beach")
                              .map((filteredDestination) => (
                                  <DestinationList destination={filteredDestination} />
                              ))
                        : destinationFilterType === "popular"
                        ? destinations
                              .filter((destination) => destination.type === "popular")
                              .map((filteredDestination) => (
                                  <DestinationList destination={filteredDestination} />
                              ))
                        : null}
                </DestinationBottom>
            </DestinationBottomContainer>
        </DestinationContainer>
    );
};

export default Destination;
const DestinationContainer = styled.section`
    padding: 60px 100px;
`;

const DestinationTop = styled.div``;
const Title = styled.h2`
    font-size: 26px;
    font-weight: 600;
    margin-bottom: 30px;
`;
const ButtonContainer = styled.div`
    border-bottom: 1px solid #cccccc;
`;
const DestinationButton = styled(Button)`
    font-weight: 600 !important;
    font-size: 13px !important;
    color: #717171 !important;
    border-bottom: 2px solid #fff !important;
    padding: 10px !important;
    text-transform: inherit !important;
    border-radius: 0 !important;
    &:hover {
        border-bottom: 2px solid #000 !important;
        color: #000 !important;
    }
    &.active {
        border-bottom: 2px solid #000 !important;
        color: #000 !important;
        background-color: rgba(0, 0, 0, 0.04);
    }
`;
const DestinationBottomContainer = styled.div`
    min-height: 300px;
    padding-top: 20px;
`;
const DestinationBottom = styled.div`
    display: flex;
    justify-content: flex-start;
    flex-wrap: wrap;
`;
